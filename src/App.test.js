import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import App from './App';
import reducer from './store/reducer';

const store = createStore(reducer);

it('renders without crashing', () => {
  shallow(<Provider store={store}><App /></Provider>);
});