import React, { Component } from "react";
import { connect } from "react-redux";

import axios from "axios";
import "./style.css";

axios.defaults.baseURL = "https://api.thecatapi.com/v1";
axios.defaults.headers.common["x-api-key"] = "DEMO-API-KEY";

class Cats extends Component {
  async getCatsImagesByCategory(category_id, amount, loadNewCategory) {
    const res = await axios(
      "/images/search?category_ids=" + category_id + "&limit=" + amount
    );
    console.table(res.data);

    let newImages = [];
    if (!loadNewCategory) {
      newImages = [...this.state.images];
    }

    this.setState({ images: newImages.concat(res.data), durum: false });
  }

  getData = () =>{
    console.log("tested");
  }

  constructor(...args) {
    super(...args);
    this.state = {
      images: [],
      durum: true
    };
  }

  componentWillReceiveProps(nextProps) {
    /*if (this.state.images.length === 0) {*/
    (async () => {
      try {
        let loadNewCategory = 1;
        if (this.props.values.s_category === nextProps.values.s_category) {
          loadNewCategory = 0;
        }
        this.setState({
          data: await this.getCatsImagesByCategory(
            nextProps.values.s_category,
            nextProps.values.s_count,
            loadNewCategory
          )
        });
      } catch (e) {
        //error...
        console.error(e);
      }
    })();
    /*  }*/
  }

  render() {
    return (
      <div>
        <div className="title">
          {!this.state.durum
            ? "Selected Category Name : " + this.props.values.s_name.toUpperCase()
            : "Cat images will be seen here"}
        </div>
        <div className="grid-container">
          {this.state.images.map(image => (
            <img key={image.url} className="cat-image" alt="" src={image.url} />
          ))}
        </div>
        <hr />
        <div>
          {!this.state.durum ? (
            <button onClick={() => this.props.onShowMoreCats()}>
              Show some more
            </button>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onShowMoreCats: () =>
      dispatch({
        type: "SHOW_MORE",
        payload: { count: 10 }
      })
  };
};
export default connect(
  null,
  mapDispatchToProps
)(Cats);
