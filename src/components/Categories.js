import React, { Component } from "react";
import { connect } from "react-redux";

import axios from "axios";

axios.defaults.baseURL = "https://api.thecatapi.com/v1";
axios.defaults.headers.common["x-api-key"] = "DEMO-API-KEY";

class Categories extends Component {
  async getCategories() {
    const res = await axios("/categories");
    return res.data;
  }

  constructor(...args) {
    super(...args);
    this.state = {
      categories: []
    };
  }

  componentDidMount() {
    if (this.state.categories.length === 0) {
      (async () => {
        try {
          this.setState({ categories: await this.getCategories() });
        } catch (e) {
          //error...
          console.error(e);
        }
      })();
    }
  }
  render() {
    return (
      <div>
        <div>{/*"selected_category :" + this.props.s_category*/}</div>
        <div>
          {this.state.categories.map(cats => (
            <div>
              <ol>
                <li
                  key={cats.id}
                  value={cats.id}
                  onClick={() => this.props.onShowSomeCats(cats.id, cats.name)}
                >
                  {cats.id === this.props.s_category ? "*" : ""}
                  {cats.name}
                </li>
              </ol>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    s_category: state.selected_category
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onShowSomeCats: (id, name) =>
      dispatch({
        type: "SHOW_SOME_CATS",
        payload: { category: id, name: name, count: 10 }
      })
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Categories);
