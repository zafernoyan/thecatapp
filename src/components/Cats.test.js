import reducer from "../store/reducer";
import axios from "axios";

describe("reducer", () => {
  const initialState = {
    selected_category: 0,
    selected_name: "",
    count: 10
  };

  it("handles SHOW_MORE as expected", () => {
    const reducerTester = reducer(initialState, {
      type: "SHOW_MORE",
      payload: {
        count: 13
      }
    });

    expect(reducerTester).toEqual({
      selected_category: 0,
      selected_name: "",
      count: 23
    });
  });
});

const fetchTestData = async () => {
  try {
    const res = await axios(
      "https://api.thecatapi.com/v1/images/search?category_ids=5&limit=1"
    );
    return res;
  } catch (e) {
    expect(e).rejects.toThrow("error");
  }
};

it("should call a fetchTestData function", done => {
  fetchTestData().then(response => {
    expect(response).toEqual({
      res: {}
    });
  });
  done();
});
