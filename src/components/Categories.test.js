import reducer from "../store/reducer";

describe("reducer", () => {
  const initialState = {
    selected_category: 0,
    selected_name: "",
    count: 10
  };

  it("handles SHOW_SOME_CATS as expected", () => {
    const reducerTester = reducer(initialState, {
      type: "SHOW_SOME_CATS",
      payload: {
        category: 1, name: "TestCategory", count: 20 
      }
    });

    expect(reducerTester).toEqual({
            selected_category: 1,
            selected_name: "TestCategory",
            count: 20
    });
  });


});
