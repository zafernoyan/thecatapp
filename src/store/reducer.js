import * as actionTypes from './actions';

const initialState = {
  selected_category: 0,
  selected_name: "",
  count: 10
};
const reducer = (state = initialState, action) => {
  // eslint-disable-next-line default-case
  switch (action.type) {
    case actionTypes.SHOW_SOME_CATS: {
      return {
        ...state,
        selected_category: action.payload.category,
        selected_name: action.payload.name,
        count: action.payload.count
      };
    }
    case actionTypes.SHOW_MORE: {
      return {
        ...state,
        count: state.count + action.payload.count
      };
    }
  }
  return state;
};

export default reducer;
