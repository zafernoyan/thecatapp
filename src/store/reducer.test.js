import reducer from "./reducer";

describe("reducer", () => {
  const initialState = {
    selected_category: 0,
    selected_name: "",
    count: 10
  };

  it("returns the initial state when an action type is not passed", () => {
    const reducerTester = reducer(undefined, {});
    expect(reducerTester).toEqual(initialState);
  });
});
