import React, { Component } from "react";
import { connect } from "react-redux";

import "./App.css";
import Categories from "./components/Categories";
import Cats from "./components/Cats";

class App extends Component {
  constructor(...args) {
    super(...args);
    this.state = {};
  }
  render() {
    return (
      <div className="container">
        <div className="header">
          <h1>This is the cat app  {/*this.props.s_count*/}</h1>
        </div>
        <div className="navigation-menu">
          <h2>Select a category below to see some cat images:</h2>
          <Categories />
        </div>
        <div className="main-content">
          <Cats values={this.props} />
        </div>
        <div className="footer"> </div>
        
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    s_category: state.selected_category,
    s_name: state.selected_name,
    s_count: state.count
  };
};

export default connect(mapStateToProps)(App);
